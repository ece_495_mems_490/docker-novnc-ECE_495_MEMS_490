#!/bin/bash
#
# get the server HOSTID
#
INPUT=/srv/persistent-data/docker-scripts/hostID
OLDIFS=$IFS
IFS=,
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read hostID
do
  MYHOSTID=$hostID
  echo $hostID:
done < $INPUT
IFS=$OLDIFS
#
# now loop through the regular user mappings
#
INPUT=/srv/persistent-data/docker-scripts/mapping-$MYHOSTID
OLDIFS=$IFS
IFS=,
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read usernum port pw
do
        echo danai-testing-01.colab.duke.edu:$port - $usernum
        sudo docker run --name docker-cs250-$port \
          -d -p 127.0.0.1\:\:6080 \
          --memory 5000M \
          -e PASSWORD=$pw \
          -e VNCPASS=$pw \
          -e DISABLE_XSRF=TRUE \
          -e VIRTUAL_HOST=danai-testing-01.colab.duke.edu\:$port \
          -e MAP_VIRTUAL_PORT=$port \
          -h danai-testing-01.colab.duke.edu \
          -t docker-cs250
        sleep 3
done < $INPUT
IFS=$OLDIFS



